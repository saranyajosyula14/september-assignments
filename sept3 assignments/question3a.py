'''
	Python program to Read the file using numpy and
    removing all the rows with NaN values
'''

import numpy as np

data = np.genfromtxt('test.csv', delimiter=',')
a = data[~(np.isnan(data).any(axis=1))]

print(a)
