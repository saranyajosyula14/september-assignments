'''
	Python program to draw a circle accoding to given equation.
'''

# import necessary modules required
import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(-10.0, 10.0, 1000)
y = np.linspace(-10.0, 10.0, 1000)

X, Y = np.meshgrid(x,y)

# store the given equation in new variable
F = X**2 + Y**2 - 1.0

# Plot the points using matplotlib
fig, ax = plt.subplots()

ax.contour(X,Y,F,[0])
ax.set_aspect(1)

plt.title('My Circle', fontsize=8)

# to display resultant figure
plt.show()
