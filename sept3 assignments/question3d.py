'''
	Python program to Read the file using numpy and
	Finding Max Values of each column
'''

import numpy as np

a = np.genfromtxt('test.csv', delimiter=',')

max_value = np.amax(a, axis = 0)

print(max_value)
