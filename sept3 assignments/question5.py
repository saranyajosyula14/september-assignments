'''
	Python program for plotting the curves in two subplots from -10 to 10 with ticks 0.5
  by providing legends and grids.
'''

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-10,10,100)
y = 5*x + 3

plt.plot(x,y, color='b')
plt.xlim(-10,10)
plt.ylim(-10,10)

plt.grid(linestyle='--')
plt.legend(loc='upper right')
plt.show()

x = np.linspace(-10,10,100)
y = x**2 + 3

plt.plot(x,y, color='b')
plt.xlim(-10,10)
plt.ylim(-10,10)

plt.grid(linestyle='--')
plt.legend(loc='upper right')

plt.show()
