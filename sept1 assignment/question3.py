import pandas as pd


def add_total_percent_columns(filepath):

	df = pd.read_csv(filepath)

	df["Total"] = df[['math', 'science','arts']].sum(axis=1) 
	df["Percentage"]= df[["Total"]].div(3).round(1)
	print(df)

	df.to_csv(filepath)

filename = input("Enter the filename : ")
filepath = filename+'.csv' 
add_total_percent_columns(filepath)
