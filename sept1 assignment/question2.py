# Python program to reverse the data in a file and also the filename

# Taking the file_name as input
file_name = input()
extention = input()
#Reversing the filename and storing the value in the new variable file_name_new
file_name_new = file_name[::-1]
f1 = open(file_name_new+extention, "w")

# Open the input file and get the content into a variable data
with open(file_name+extention, "r") as myfile:
	data = myfile.read()

# For Full Reversing we will store the value of 
# data into new variable resultant_data
# in a reverse order
resultant_data = data[::-1]

#writing the resultant_data data into the newfile
f1.write(resultant_data)

f1.close()
