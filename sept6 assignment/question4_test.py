import pytest
from question4 import add,divide

def test_add():
    x=add(1,2)
    assert x==3
    assert add(1,2)==3

def test_divide():
    assert divide(2,2)==1
    assert divide(1,2)==0.5
    
