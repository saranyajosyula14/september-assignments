import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')

df = pd.read_csv("breastCancerDataWisconsin.csv")

df.head()

df.tail()

df.shape

df.describe()

import sklearn

from sklearn.model_selection import train_test_split as tts

X = df.drop("diagnosis",axis = 1)
X.shape

Y = df["diagnosis"]

X_train,X_test,Y_train,Y_test = tts(X,Y,test_size = 0.2,train_size = 0.8)

X_train.shape

from sklearn.linear_model import LogisticRegression

classifier = LogisticRegression()
classifier.fit(X_train, Y_train)

y_pred = classifier.predict(X_test)

from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

print(accuracy_score(Y_test, y_pred)*100)

print(confusion_matrix(Y_test, y_pred))

print(classification_report(Y_test,y_pred))

