'''
Random Password Generator using Python
'''

#import the necessary modules!
import random
import string

## characters to generate password from
alphabets = list(string.ascii_letters)
digits = list(string.digits)
special_characters = list("!@#$%^&*()")
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()")

print('hello, Welcome to Password generator!')

class password_generator_class:
    def password_generator(self):
        # 
        ## length of password from the user
        length = int(input("Enter password length: "))

        ## number of character types
        alphabets_count = int(input("Enter alphabets count in password: "))
        digits_count = int(input("Enter digits count in password: "))
        special_characters_count = int(input("Enter special characters count in password: "))

        characters_count = alphabets_count + digits_count + special_characters_count

        ## check the total length with characters sum count
        ## print not valid if the sum is greater than length
        if characters_count > length:
            print("Characters total count is greater than the password length")
            return


        ## initializing the password
        password = []
        
        ## picking random alphabets
        for i in range(alphabets_count):
            password.append(random.choice(alphabets))


        ## picking random digits
        for i in range(digits_count):
            password.append(random.choice(digits))


        ## picking random alphabets
        for i in range(special_characters_count):
            password.append(random.choice(special_characters))


        ## if the total characters count is less than the password length
        ## add random characters to make it equal to the length
        if characters_count < length:
            random.shuffle(characters)
            for i in range(length - characters_count):
                password.append(random.choice(characters))


        ## shuffling the resultant password
        random.shuffle(password)

        ## converting the list to string
        ## printing the list
        print("New generated password with given parameters is : "+"".join(password))
            
    def printStrongNess(self,*args):
        for x in args:
            n = len(x)

            # Checking lower alphabet in string
            hasLower = False
            hasUpper = False
            hasDigit = False
            specialChar = False
            normalChars = "abcdefghijklmnopqrstu"
            "vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "
            
            for i in range(n):
                if x[i].islower():
                    hasLower = True
                if x[i].isupper():
                    hasUpper = True
                if x[i].isdigit():
                    hasDigit = True
                if x[i] not in normalChars:
                    specialChar = True
        
            # Strength of password
            print("Strength of password for "+x+"is : ", end = "")
            if (hasLower and hasUpper and
                hasDigit and specialChar and n >= 8):
                print("Strong")
                
            elif ((hasLower or hasUpper) and
                specialChar and n >= 6):
                print("Moderate")
            else:
                print("Weak")

if __name__=="__main__":
    res = password_generator_class()
    res.password_generator()
    res.printStrongNess('ABCbc@@hvuisbuiz2323213',"Hello@2001")
